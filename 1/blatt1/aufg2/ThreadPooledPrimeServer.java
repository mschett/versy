package blatt1.aufg2;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import blatt1.ServerEndpoint;
import blatt1.ServerEndpoint.Request;

public class ThreadPooledPrimeServer {
	private final static int NUMTHREADS = 2;
	
	//sync deqeue mit requests
	private BlockingDeque<ServerEndpoint.Request> taskList = new LinkedBlockingDeque<ServerEndpoint.Request>();
	
	private final ServerEndpoint endpoint;

	ThreadPooledPrimeServer() {
		endpoint = new ServerEndpoint();
	}

	private boolean isPrime(long number) {
		for (long i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	class Worker implements Runnable {

		@Override
		public void run() {
			while(true){
				Request req;
				//System.out.println(taskList.size());
				try {
					synchronized (taskList) {
						req = taskList.takeFirst();
					}
					boolean prime = isPrime(req.getNumber());
					endpoint.send(req.getSender(), prime);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	void run() {
		ExecutorService executors = Executors.newFixedThreadPool(NUMTHREADS);
		System.out.println("Serial PrimeServer up and running...");
		for(int i = 0; i < NUMTHREADS; i++){
			executors.execute(new Worker());
		}

		executors.shutdown();
		//executors.shutdown();
		while (true) {
			ServerEndpoint.Request request = endpoint.blockingReceive();
			taskList.add(request);
		}
	}

	public static void main(String[] args) {
		new ThreadPooledPrimeServer().run();
	}

}
