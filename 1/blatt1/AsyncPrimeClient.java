package blatt1;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.TimeUnit;

public class AsyncPrimeClient {

	ClientEndpoint endpoint = new ClientEndpoint();
	SocketAddress server = new InetSocketAddress("localhost", 4711);

	private final long number;

	private boolean isReady = false;
	private boolean isPrime = false;

	AsyncPrimeClient(long number) {
		this.number = number;
	}

	private class WaitForReceive implements Runnable {

		@Override
		public void run() {
			isPrime = endpoint.blockingReceive();
			isReady = true;
		}
	}

	public void run() {

		endpoint.send(server, number);
		System.out.print("Die Zahl " + number + " ist ");
		Thread thread = new Thread(new WaitForReceive());
		thread.start();
		while (!isReady) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.print(".");
		}
		System.out.print((isPrime ? " eine " : " keine ") + "Primzahl\n");
		
		
	}

	public static void main(String[] args) {
		for (long i = 1000000000000000000L; i < 1000000000000000010L; i++) {
			new AsyncPrimeClient(i).run();
		}
	}
}
