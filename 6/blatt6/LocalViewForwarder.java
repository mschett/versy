package blatt6;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;

import forum.framework.IForumModel;
import forum.framework.IForumView;

public class LocalViewForwarder implements IForumModel {

	private static LocalViewReceiver localViewReceiver = null;

	public LocalViewForwarder() {
		localViewReceiver = LocalViewReceiver.getLocalViewReceiver();
	}

	@Override
	public void deregisterView(String arg0) throws NotBoundException,
			IOException {
		localViewReceiver.deregisterView(arg0);

	}

	@Override
	public void moveEast(String arg0) throws NotBoundException, IOException {
		localViewReceiver.moveEast(arg0);

	}

	@Override
	public void moveNorth(String arg0) throws NotBoundException, IOException {
		localViewReceiver.moveNorth(arg0);

	}

	@Override
	public void moveSouth(String arg0) throws NotBoundException, IOException {
		localViewReceiver.moveSouth(arg0);

	}

	@Override
	public void moveWest(String arg0) throws NotBoundException, IOException {
		localViewReceiver.moveWest(arg0);

	}

	@Override
	public void registerView(String arg0, IForumView arg1)
			throws AlreadyBoundException, IOException {
		localViewReceiver.registerView(arg0, arg1);

	}

}
