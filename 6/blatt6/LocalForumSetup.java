package blatt6;

import forum.framework.ForumClient;
//import forum.framework.ForumServer;

public class LocalForumSetup {

	public static void main(String[] args) {

//		ForumServer forumServer = new ForumServer(
//				LocalModelReceiver.getLocalModelReceiver());

		try {
			ForumClient forumClient1 = new ForumClient(
					new LocalModelForwarder());
			ForumClient forumClient2 = new ForumClient(
					new LocalModelForwarder());
			forumClient1.register();
			forumClient2.register();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
