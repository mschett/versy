package blatt6;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;

import forum.framework.ForumModel;
import forum.framework.IForumModel;
import forum.framework.IForumView;

public class LocalViewReceiver implements IForumModel, Runnable {

	private static LocalViewReceiver localViewReceiver = null;
	private static ForumModel forumModel = null;

	private LocalViewReceiver() {
		forumModel = ForumModel.INSTANCE;
	}

	@Override
	public void deregisterView(String arg0) throws NotBoundException,
			IOException {
		forumModel.deregisterView(arg0);

	}

	@Override
	public void moveEast(String arg0) throws NotBoundException, IOException {
		forumModel.moveEast(arg0);

	}

	@Override
	public void moveNorth(String arg0) throws NotBoundException, IOException {
		forumModel.moveNorth(arg0);

	}

	@Override
	public void moveSouth(String arg0) throws NotBoundException, IOException {
		forumModel.moveSouth(arg0);

	}

	@Override
	public void moveWest(String arg0) throws NotBoundException, IOException {
		forumModel.moveWest(arg0);

	}

	@Override
	public void registerView(String arg0, IForumView arg1)
			throws AlreadyBoundException, IOException {
		forumModel.registerView(arg0, arg1);

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	public static LocalViewReceiver getLocalViewReceiver() {
		if (localViewReceiver == null) {
			localViewReceiver = new LocalViewReceiver();
		}
		return localViewReceiver;
	}

}
