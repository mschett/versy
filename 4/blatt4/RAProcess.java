package blatt4;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import disy.Process;

public class RAProcess extends Process {

	private static final int RELEASED = 0;
	private static final int WANTED = 1;
	private static final int HELD = 2;

	private Queue<RARequest> waitingRequests = new LinkedList<RARequest>();

	CountDownLatch latch;

	private int state;
	private int logicalTime;
	private int requestTime;

	protected RAProcess(int id) {
		super(id);
		this.state = RELEASED;
		this.logicalTime = 0;
	}

	public void process(RARequest request) {
		this.logicalTime = request.getLogicalTime() + 1;
		if (this.state == HELD
				|| (this.state == WANTED && (request.getLogicalTime() > this.requestTime))) {
			waitingRequests.add(request);
		} else {
			request.getSender().receiveMessage(
					new RAResponse(this, logicalTime));
		}
	}

	public void process(RAResponse response) {
		this.logicalTime = response.getLogicalTime() + 1;
		latch.countDown();
	}

	public void process(RATrigger trigger) {
		this.requestTime = this.logicalTime;
		this.state = WANTED;
		latch = new CountDownLatch(super.destinations.size());
		this.multicast(new RARequest(this, this.logicalTime));
		try {
			latch.await();
			this.state = HELD;
			System.out.println("process " + this.getID() + " holds lock");
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.state = RELEASED;
		System.out.println("process " + this.getID() + " releases lock");
		for (RARequest p : waitingRequests) {
			p.getSender()
					.receiveMessage(new RAResponse(this, this.logicalTime));
		}
	}

}
