package blatt3;

import java.util.Map;
import java.util.TreeMap;

public class CompositeDirectory extends Directory {
	private Map<Integer, Directory> leafs = new TreeMap<Integer, Directory>();

	protected CompositeDirectory(CompositeDirectory parent) {
		super(parent);

	}

	@Override
	Cell lookup(int entity) {
		Directory leaf = leafs.get(entity);
		if (leaf == null) {
			return parent.lookup(entity);
		}
		return leaf.lookup(entity);
	}

	public void add(int entity, Directory dir) {
		leafs.put(entity, dir);
		if (parent != null) {
			parent.add(entity, this);
		}
	}

}
