package blatt5;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import disy.Process;

public class BullyProcess extends Process {

	private volatile UUID uuid;
	private List<Process> requests = new LinkedList<Process>();
	
	public BullyProcess(int id) {
		super(id);
	}

	public void process(ElectionTrigger trigger) {
		uuid = UUID.randomUUID();
		this.receiveMessage(new ElectionRequest(this, uuid));
	}
	
	public void process(ElectionRequest request){
		if(request.getUuid() == uuid && request.getSender() != this){
			request.getSender().receiveMessage(new ElectionResponse(this));
			return;
		}
		if(request.getSender().getID() < this.getID()){
			request.getSender().receiveMessage(new ElectionResponse(this));
		}
		Process p;
		for (Map.Entry<Integer, Process> i : destinations.entrySet()) {
			if(i.getKey() > this.getID()){
				p = i.getValue();
				synchronized (requests) {
					requests.add(p);
					p.receiveMessage(new ElectionRequest(this, uuid));
				}
				
			}
		}
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		synchronized (requests) {
			if(!requests.isEmpty()){
				System.out.println("Brace yourself! \nI am the chiefmaster " + this);
			}		
		}
	}
	
	public void process(ElectionResponse response){
		synchronized (requests) {
			requests.clear();
		}
	}
}
