package blatt2;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JOptionPane;

import blatt1.ClientEndpoint;
import blatt1.ServerEndpoint;
import blatt1.ServerEndpoint.Request;

public class ThreadPooledPrimeServer {

	ExecutorService executors;

	private final static int NUMTHREADS = 2;
	private static final long POISON_PILL = -1;

	private final ServerEndpoint endpoint;

	ThreadPooledPrimeServer() {
		endpoint = new ServerEndpoint();
		run();
	}

	private boolean isPrime(long number) {
		for (long i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	class Worker implements Runnable {

		private Request req;

		private Worker(Request req) {
			this.req = req;
		}

		@Override
		public void run() {

			boolean prime = isPrime(req.getNumber());
			endpoint.send(req.getSender(), prime);

		}
	}

	class KillerThread implements Runnable {

		@Override
		public void run() {
			SocketAddress address = new InetSocketAddress("localhost", 4711);
			ClientEndpoint clientEndpoint = new ClientEndpoint();
			JOptionPane.showMessageDialog(null, "Wanna kill the Server?",
					"Server kill", JOptionPane.QUESTION_MESSAGE);
			clientEndpoint.send(address, POISON_PILL);
			System.out.println("Poison Pill send!");

		}

	}

	void run() {
		executors = Executors.newFixedThreadPool(NUMTHREADS);
		System.out.println("Serial PrimeServer up and running...");
		new Thread(new KillerThread()).start();

		while (true) {
			ServerEndpoint.Request request = endpoint.blockingReceive();
			if (request.getNumber() == POISON_PILL) {
				executors.shutdown();
				break;
			}
			executors.execute(new Worker(request));
		}
	}

	public static void main(String[] args) {
		new ThreadPooledPrimeServer();
	}

}
